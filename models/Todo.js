var mongoose = require('mongoose');

var TodoSchema = new mongoose.Schema({
	id: Number,
	name: String,
	description: String,
	date: String,
	time: String,
	done: Boolean,
	deleted: Boolean
});

module.exports = mongoose.model('Todo', TodoSchema);
